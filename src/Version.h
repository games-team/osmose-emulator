/*
 * Copyright holder 2001-2011 Vedder Bruno.
 * Work continued by 2016-2020 Carlos Donizete Froes [a.k.a coringao]
 *
 * This file is part of Osmose Emulator, a Sega Master System and Game Gear
 * software emulator.
 *
 * Osmose Emulator is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Osmose Emulator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Osmose Emulator. If not, see <http://www.gnu.org/licenses/>.
 *
 * Many thanks to Vedder Bruno, the original author of Osmose Emulator.
 *
 */

#ifndef VERSION_H
#define VERSION_H

// These four definitions should be coherent!
#define __OSMOSE_VERSION__ "Osmose Emulator"
#define MAJOR  1
#define MIDDLE 6
#endif
